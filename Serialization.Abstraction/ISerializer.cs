﻿namespace Serialization.Abstraction
{
    public interface ISerializer<out TOut>
    {
        TOut Serialize<TIn>(TIn serializable);
    }
}
