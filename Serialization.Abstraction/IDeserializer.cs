﻿namespace Serialization.Abstraction
{
    public interface IDeserializer<in TIn>
    {
        TOut Deserialize<TOut>(TIn serializedObject);
    }
}
