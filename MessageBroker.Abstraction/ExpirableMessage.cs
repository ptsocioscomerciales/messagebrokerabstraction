﻿namespace MessageBroker.Abstraction
{
   public abstract class ExpirableMessage
   {
      public int MessageTTL { get; set; }
   }
}
