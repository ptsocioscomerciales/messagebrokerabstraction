﻿namespace MessageBroker.Abstraction
{
   public interface IProducer
   {
      void Send<TMessage>(TMessage message) where TMessage : ExpirableMessage;
   }
}
