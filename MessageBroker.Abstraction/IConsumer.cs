﻿namespace MessageBroker.Abstraction
{
   public interface IConsumer
   {
      T Recibe<T>();
   }
}
