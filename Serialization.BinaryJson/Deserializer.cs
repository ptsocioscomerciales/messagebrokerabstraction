﻿using System.Text;
using Newtonsoft.Json;
using Serialization.Abstraction;

namespace Serialization.BinaryJson
{
    public class Deserializer : IDeserializer<byte[]>
    {
        public T Deserialize<T>(byte[] serializedObject)
        {
            var message = Encoding.UTF8.GetString(serializedObject);

            var deserialized = JsonConvert.DeserializeObject<T>(message);

            return deserialized;
        }
    }
}
