﻿using System.Text;
using Newtonsoft.Json;
using Serialization.Abstraction;

namespace Serialization.BinaryJson
{
    public class Serializer: ISerializer<byte[]>
    {
        public byte[] Serialize<T>(T serializable)
        {
            var serialized = JsonConvert.SerializeObject(serializable);

            var encoded = Encoding.UTF8.GetBytes(serialized);

            return encoded;
        }
    }
}
